<?php


namespace App\Services;


use App\Models\SitesInquiry;

class SitesInquiryService
{
    /**
     * @param $inquiry_id
     * 询盘单批量分配
     */
    public static function inquiry_batch($inquiry_id)
    {
        $inquiry = SitesInquiry::where('id', $inquiry_id)->first();
        if (!empty($inquiry)) {
            $inquirys = SitesInquiry::where('id', $inquiry_id)->get();
            foreach ($inquirys as $inquiry) {
                if ($inquiry['inquiry_state'] == 1) {
                    $inquiry->staff_id = null;
                    $inquiry->save();
                } else {
                    return $inquiry['inquiry_state'];
                }
            }
        }
    }


    public static function inquiry_batch_rollback($inquiry_id)
    {
        $inquiry = SitesInquiry::where('id', $inquiry_id)->first();
        if (!empty($inquiry)) {
            $inquirys = SitesInquiry::where('id', $inquiry_id)->get();
            foreach ($inquirys as $inquiry) {
                if ($inquiry['inquiry_state'] == 1) {
                    $inquiry->staff_id = null;
                    $inquiry->inquiry_state = '0';
                    $inquiry->save();
                } else {
                    return $inquiry['inquiry_state'];
                }
            }
        }
    }
}
