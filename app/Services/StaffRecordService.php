<?php


namespace App\Services;


use App\Models\StaffRecord;

class StaffRecordService
{
    public static function deleteStaff($staff_id)
    {
        $staff = StaffRecord::where('id', $staff_id)->first();
        $staff->delete();
    }
}
