<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

/**
 * Class StaffRecord
 * @package App\Models
 * @method static where(string $key, string $value)
 */

class StaffRecord extends Model
{
	use HasDateTimeFormatter;
    use SoftDeletes;

    protected $table = 'staff_records';

    /**
     * 雇员记录有一个组织部门记录
     * @return HasOne
     */
    public function department(): HasOne
    {
        return $this->hasOne(StaffDepartment::class, 'id', 'department_id');
    }
}
