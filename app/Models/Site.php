<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
	use HasDateTimeFormatter;
    use SoftDeletes;

    /**
     * @return HasOne
     * 网站分类
     */
    public function category() :HasOne
    {
        return $this->hasOne(SitesCategory::class,'id','category_id');
    }
}
