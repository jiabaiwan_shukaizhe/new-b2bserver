<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class SitesInquiry extends Model
{
    use HasDateTimeFormatter;
    use SoftDeletes;

    protected $table = 'sites_inquiry';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     * 询盘单的来源网站
     */
    public function site(): HasOne
    {
        return $this->hasOne(Site::class, 'id', 'site_id');
    }

    /**
     * @return HasOne
     * 询盘单分配业务员
     */
    public function staff(): HasOne
    {
        return $this->hasOne(StaffRecord::class, 'id', 'staff_id');
    }
}
