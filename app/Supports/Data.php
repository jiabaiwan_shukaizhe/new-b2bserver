<?php


namespace App\Supports;

use Dcat\Admin\Widgets\Card;


class Data
{
    /**
     * @param $data
     * @param string $msg
     * @return mixed|string
     * 空数据字段显示处理
     */
    public static function empty_data_expand($data,$msg='无') : string
    {
        return $data = isset($data)||empty($data) ? $msg : $data;
    }

    /**
     * @param $data
     * @param int $text_length
     * @return mixed|string
     * 超长内容显示处理，默认50个字符
     */
    public static function overlength_content_display_inlist($data,$text_length=50) : string
    {
        return $data = strlen($data) > $text_length ? substr($data, 0, $text_length) . "......" : $data;
    }

    /**
     * @param $title
     * @param $data
     * @return string
     * 超长内容展开卡片
     */
    public static function overlength_content_expand_card($title,$data) : string
    {
        $data = new Card($title,$data);
        return "<div style='padding:10px 10px 0'>$data</div>";
    }

    /**
     * @param $url
     * @return string
     */
    public static function open_url($url) : string
    {
        return "<a href='$url' target='_blank'>$url</a>";
    }

    /**
     * @return string[]
     * 询盘单状态
     */
    public static function inquiry_state() : array
    {
        return [
            0 => '未分配',
            1 => '已分配',
        ];
    }

    /**
     * @return string[]
     * 询盘单状态颜色
     */

    public static function inquiry_state_label_color() : array
    {
        return [
            'default' => 'primary',
            0 => 'primary',
            1 => 'danger',
        ];
    }

    /**
     * @return string[]
     * 性别
     */
    public static function genders(): array
    {
        return [
            '未知' => '未知',
            '男' => '男',
            '女' => '女'
        ];
    }
}
