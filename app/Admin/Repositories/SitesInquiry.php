<?php

namespace App\Admin\Repositories;

use App\Models\SitesInquiry as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class SitesInquiry extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
