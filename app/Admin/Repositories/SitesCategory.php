<?php

namespace App\Admin\Repositories;

use App\Models\SitesCategory as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class SitesCategory extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
