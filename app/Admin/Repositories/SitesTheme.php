<?php

namespace App\Admin\Repositories;

use App\Models\SitesTheme as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class SitesTheme extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
