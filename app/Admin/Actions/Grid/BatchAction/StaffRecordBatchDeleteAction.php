<?php

namespace App\Admin\Actions\Grid\BatchAction;

use App\Services\StaffRecordService;
use Dcat\Admin\Actions\Response;
use Dcat\Admin\Grid\BatchAction;
use Illuminate\Http\Request;

class StaffRecordBatchDeleteAction extends BatchAction
{
    /**
     * @return string
     */
    protected $title = '🔨 批量删除雇员';

    /**
     * Handle the action request.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function handle(): Response
    {
        $keys = $this->getKey();

        foreach ($keys as $key) {
            StaffRecordService::deleteStaff($key);
        }

        return $this->response()->success('批量删除雇员成功！')->refresh();
    }

    /**
     * @return string|array|void
     */
    public function confirm()
    {
        return '确定批量删除雇员吗？';
    }
}
