<?php

namespace App\Admin\Actions\Grid\BatchAction;

use App\Services\SitesInquiryService;
use Dcat\Admin\Actions\Response;
use Dcat\Admin\Grid\BatchAction;
use Dcat\Admin\Traits\HasPermissions;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class DistributionBatchSalesmanAction extends BatchAction
{
    /**
     * @var
     */
    protected $action;
    /**
     * @return string
     */
    protected $title = '👨‍💼 批量反分配';

    /**
     * Handle the action request.
     *
     * @return Response
     */
    public function handle()
    {
        $keys = $this->getKey();
        foreach ($keys as $key) {
            SitesInquiryService::inquiry_batch_rollback($key);
        }
        return $this
            ->response()
            ->success('批量反分配成功')
            ->refresh();
    }

    /**
     * @return string|array|void
     */
    public function confirm()
    {
        return '您确定要反分配选中的询盘单吗？';
    }

    /**
     * @param Model|Authenticatable|HasPermissions|null $user
     *
     * @return bool
     */
    protected function authorize($user): bool
    {
        return true;
    }

    /**
     * @return array
     */
    protected function parameters(): array
    {
        return [];
    }
}
