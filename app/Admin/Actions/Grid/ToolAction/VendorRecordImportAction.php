<?php

namespace App\Admin\Actions\Grid\ToolAction;

use App\Admin\Forms\VendorRecordImportForm;
use Dcat\Admin\Grid\Tools\AbstractTool;
use Dcat\Admin\Widgets\Modal;

class VendorRecordImportAction extends AbstractTool
{
    /**
     * @return string
     */
    protected $title = '导入';

    public function render()
    {
        return Modal::make()
            ->lg()
            ->body(new VendorRecordImportForm())
            ->button("<a class='btn btn-primary' style='color: #4c60a3;'><i class='feather icon-package'></i>&nbsp;&nbsp;$this->title</a>");
    }
}
