<?php

namespace App\Admin\Actions\Grid;

use App\Admin\Forms\DistributionSalesman;
use Dcat\Admin\Grid\RowAction;
use Dcat\Admin\Widgets\Modal;

class DistributionSalesmanAction extends RowAction
{
    /**
     * @return string
     */
	protected $title = '👨‍💼 分配业务员';

    public function render()
    {
        $from = DistributionSalesman::make()->payload(['id'=>$this->getKey()]);

        return Modal::make()
            ->lg()
            ->title('为 ' . $this->getRow()->customer_realname . ' 分配业务员')
            ->body($from)
            ->button($this->title);
    }
}
