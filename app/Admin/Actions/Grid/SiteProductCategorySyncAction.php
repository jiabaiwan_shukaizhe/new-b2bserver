<?php

namespace App\Admin\Actions\Grid;

use App\Services\SiteProductCategorySyncService as SPCS;
use Dcat\Admin\Actions\Response;
use Dcat\Admin\Grid\RowAction;
use Illuminate\Http\Request;

class SiteProductCategorySyncAction extends RowAction
{
    /**
     * @return string
     */
    protected $title = '🍺 分类同步';

    /**
     * Handle the action request.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function handle()
    {
        $domian = SPCS::site_cate_sync($this->getKey());
        $is_sync = SPCS::is_closed($this->getKey());

        if ($domian != 'error link'&&$is_sync) {
            return $this->response()
                ->success('分类同步成功' . $domian);
        } else {
            return $this->response()->error('接口连接失败');
        }
    }

    /**
     * @return string|array|void
     */
    public function confirm()
    {
        return ['是否同步网站' . $this->getRow()->name . '的产品分类'];
    }
}
