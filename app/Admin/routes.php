<?php

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use Dcat\Admin\Admin;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index');

    /**
     * 网站管理
     */

    $router->resource('/site', 'SiteController',['names'=> [
        'index'=> 'site.index',
        'show'=> 'site.show',
    ]]);

    /**
     * 网站分类
     */

    $router->resource('sites/categories', 'SitesCategoryController',['names'=> [
        'index'=> 'sites.categories.index',
        'show'=> 'sites.categories.show',
    ]]);

    /**
     * 主题
     */

    $router->resource('sites/themes', 'SitesThemeController',['names'=> [
        'index'=> 'sites.themes.index',
        'show'=> 'sites.themes.show',
    ]]);

    /**
     * 询盘列表
     */

    $router->resource('sites/inquiry', 'SitesInquiryController',['names'=> [
        'index'=> 'sites.inquiry.index',
        'show'=> 'sites.inquiry.show',
    ]]);

    /**
     * 组织管理
     */
    $router->resource('/staff/records', 'StaffRecordController', ['names' => [
        'index' => 'staff.records.index'
    ]]);
    $router->resource('/staff/departments', 'StaffDepartmentController');

    $router->resource('/user', 'UserController',['names'=> [
        'index'=> 'site.index',
        'show'=> 'site.show',
    ]]);
});
