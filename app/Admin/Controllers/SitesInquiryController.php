<?php

namespace App\Admin\Controllers;

use App\Admin\Actions\Grid\BatchAction\DistributionBatchSalesmanAction;
use App\Admin\Actions\Grid\DistributionSalesmanAction;
use App\Admin\Repositories\SitesInquiry;
use App\Supports\Data;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Http\Controllers\AdminController;
use Dcat\Admin\Show;


class SitesInquiryController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new SitesInquiry(['site','staff']), function (Grid $grid) {
            $grid->column('id')->sortable();
            $grid->column('site.name', '网站')->filter(
                Grid\Column\Filter\Equal::make()->valueFilter()
            );
            $grid->column('inquiry_state')
                ->using(Data::inquiry_state())
                ->label(Data::inquiry_state_label_color())
                ->filter(Grid\Column\Filter\In::make(Data::inquiry_state())
                );;
            $grid->column('customer_realname');
            $grid->column('email');
            $grid->column('phone');
            $grid->column('inquiry_info')->if(function ($column) {
                if (strlen($this->inquiry_info) > 50) {
                    return $column->display(function ($inquiry_info) {
                        return Data::overlength_content_display_inlist($inquiry_info);
                    })->expand(function (Grid\Displayers\Expand $expand) {
                        return Data::overlength_content_expand_card('姓名：'.$this->customer_realname,'<b>询盘内容：</b>'.$this->inquiry_info);
                    });;
                }
            });
            $grid->column('inquiry_quantity');
            $grid->column('source_country')->filter(
                Grid\Column\Filter\Equal::make()->valueFilter()
            );
            $grid->column('user_ipaddr')->display(function ($user_ipaddr){
                return "<a href=https://ip.tool.chinaz.com/$user_ipaddr target='_blank'>$user_ipaddr</a>";
            })->help('点击网址跳转到站长工具查询ip');
            $grid->column('user_ipaddr_type')->display(function(){
                return Data::empty_data_expand($this->user_ipaddr_type);
            });
            $grid->column('business_note')->editable(true);
            $grid->column('source_url')->display(function ($source_url){
                return Data::open_url($source_url);
            });
            $grid->column('staff.name','业务员')->width('100');
            $grid->actions(function (Grid\Displayers\Actions $actions){
                if($this->inquiry_state == 0){
                    $actions->append(new DistributionSalesmanAction());
                }
            });

            $grid->batchActions([
                new DistributionBatchSalesmanAction(),
            ]);

            $grid->export();
            $grid->selector(function (Grid\Tools\Selector $selector) {
                $selector->select('inquiry_state', '状态', Data::inquiry_state());
            });
            $grid->quickSearch('customer_realname', 'email', 'phone', 'inquiry_info')->placeholder('快速搜索...');
            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('id');
            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new SitesInquiry(), function (Show $show) {
            $show->field('id');
            $show->field('site_id');
            $show->field('inquiry_id');
            $show->field('inquiry_state');
            $show->field('is_trash');
            $show->field('customer_realname');
            $show->field('email');
            $show->field('phone');
            $show->field('inquiry_info');
            $show->field('inquiry_quantity');
            $show->field('form_agent');
            $show->field('user_ipaddr');
            $show->field('user_ipaddr_type');
            $show->field('business_note');
            $show->field('addtime');
            $show->field('source_url');
            $show->field('create_by');
            $show->field('operator_by');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new SitesInquiry(), function (Form $form) {
            $form->display('id');
            $form->text('site_id');
            $form->text('inquiry_id');
            $form->text('inquiry_state');
            $form->text('is_trash');
            $form->text('customer_realname');
            $form->text('email');
            $form->text('phone');
            $form->text('inquiry_info');
            $form->text('inquiry_quantity');
            $form->text('form_agent');
            $form->text('user_ipaddr');
            $form->text('user_ipaddr_type');
            $form->text('business_note');
            $form->text('addtime');
            $form->text('source_url');
            $form->text('create_by');
            $form->text('operator_by');

            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
