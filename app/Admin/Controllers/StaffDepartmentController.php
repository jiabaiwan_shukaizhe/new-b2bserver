<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\StaffDepartment;
use Dcat\Admin\Form;
use Dcat\Admin\Http\Controllers\AdminController;
use Dcat\Admin\Layout\Content;
use Dcat\Admin\Layout\Row;
use Dcat\Admin\Show;
use Dcat\Admin\Tree;

class StaffDepartmentController extends AdminController
{
    public function index(Content $content) : Content
    {
        return $content
            ->title($this->title())
            ->description(trans('admin.list'))
            ->body(function (Row $row){
                $row->column(12, $this->treeView());
            });
    }

    public function treeView(): Tree
    {
        return new Tree(new \App\Models\StaffDepartment(), function (Tree $tree) {
            $tree->branch(function ($branch) {
                $display = "{$branch['name']}";
                if ($branch['focus_tag'] === 1) {
                    $display = "<span class='badge badge-primary mr-1'>标签</span>" . $display;
                }
                return $display;
            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new StaffDepartment(), function (Show $show) {
            $show->field('id');
            $show->field('name');
            $show->field('description');
            $show->field('parent_id');
            $show->field('order');
            $show->field('focus_tag');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new StaffDepartment(), function (Form $form) {
            $form->display('id');
            $form->text('name');
            $form->text('description');
            $form->select('parent_id', '父级部门')
                ->options(\App\Models\StaffDepartment::all()
                    ->pluck('name', 'id'))
                ->default(0);
            $form->text('order');
            $form->text('focus_tag');

            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
