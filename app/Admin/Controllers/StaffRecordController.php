<?php

namespace App\Admin\Controllers;

use App\Admin\Actions\Grid\BatchAction\StaffRecordBatchDeleteAction;
use App\Admin\Repositories\StaffRecord;
use App\Models\StaffDepartment;
use App\Supports\Data;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class StaffRecordController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new StaffRecord(['department']), function (Grid $grid) {
            $grid->column('id')->sortable();
            $grid->column('name')->display(function ($name) {
                if ($this->ma_tag === 1) {
                    return "<span class='badge badge-primary mr-1'>经理</span>$name";
                }
                return $name;
            });
            $grid->column('department.name','部门');
            $grid->column('gender');
            $grid->column('title');
            $grid->column('mobile');
            $grid->column('email');

            $grid->enableDialogCreate();
            $grid->disableDeleteButton();
            $grid->disableBatchDelete();

            $grid->batchActions([
                new StaffRecordBatchDeleteAction()
            ]);

            $grid->quickSearch('id', 'name', 'department.name', 'gender', 'title', 'mobile', 'email')
                ->placeholder('试着搜索一下')
                ->auto(false);

            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('id');
            });

            $grid->export();
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new StaffRecord(), function (Show $show) {
            $show->field('id');
            $show->field('name');
            $show->field('department_id');
            $show->field('gender');
            $show->field('title');
            $show->field('mobile');
            $show->field('email');
            $show->field('ma_tag');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new StaffRecord(), function (Form $form) {
            $form->display('id');
            $form->text('name');
            $form->select('department_id', '部门')
                ->options(StaffDepartment::selectOptions())
                ->required();;
            $form->select('gender')
                ->options(Data::genders())
                ->required();;
            $form->text('title');
            $form->text('mobile');
            $form->text('email');
            $form->text('ma_tag');

            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
