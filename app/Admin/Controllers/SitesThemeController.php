<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\SitesTheme;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class SitesThemeController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new SitesTheme(), function (Grid $grid) {
            $grid->column('id')->sortable();
            $grid->column('name');
            $grid->column('description');
            $grid->column('rules');
            $grid->column('termination');
            //$grid->column('created_at');
            //$grid->column('updated_at')->sortable();

            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('id');

            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new SitesTheme(), function (Show $show) {
            $show->field('id');
            $show->field('name');
            $show->field('description');
            $show->field('rules');
            $show->field('termination');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new SitesTheme(), function (Form $form) {
            $form->display('id');
            $form->text('name');
            $form->textarea('description')->rows(3);
            $form->text('rules');
            $form->date('termination');

            $form->hidden('created_at');
            $form->hidden('updated_at');
        });
    }
}
