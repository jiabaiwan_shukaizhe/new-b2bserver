<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\SitesCategory;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Layout\Content;
use Dcat\Admin\Show;
use Dcat\Admin\Layout\Row;
use Dcat\Admin\Http\Controllers\AdminController;
use Dcat\Admin\Tree;
use App\Models\SitesTheme as SitesThemeModel;
use App\Models\SitesCategory as SitesCategoryModel;


class SitesCategoryController extends AdminController
{
    public function index(Content $content) : Content
    {
        return $content
            ->title($this->title())
            ->description(trans('admin.list'))
            ->body(function (Row $row){
                $row->column(12, $this->treeView());
            });
    }

    protected function treeView() : Tree
    {
        return new Tree(new SitesCategoryModel,function (Tree $tree){
            $tree->tools(function (Tree\Tools $tools){

            });
        });
    }
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid(): Grid
    {
        return Grid::make(new SitesCategory(['parent', 'themes']), function (Grid $grid) {
            $grid->column('id');
            $grid->column('name');
            $grid->column('description');
            $grid->column('parent.name');
            $grid->column('themes.name');

            $grid->actions(function (Grid\RowAction $action){

            });

            $grid->enableDialogCreate();

            $grid->toolsWithOutline(false);

            $grid->quickSearch('id', 'name', 'description')
                ->placeholder('试着搜索一下')
                ->auto(false);
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new SitesCategory(['parent','themes']), function (Show $show) {
            $show->field('id');
            $show->field('parent.name');
            $show->field('description');
            $show->field('site_theme_ids');
            $show->field('parent_id');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new SitesCategory(['parent','themes']), function (Form $form) {
            $form->display('id');
            $form->text('name')->required();
            $form->text('description');
            $form->select('site_theme_ids',admin_trans_label('Themes'))
                ->options(SitesThemeModel::all()
                    ->pluck('name','id'));
            $form->select('parent_id',admin_trans_label('Parent'))
                ->options(\App\Models\SitesCategory::selectOptions());

            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
