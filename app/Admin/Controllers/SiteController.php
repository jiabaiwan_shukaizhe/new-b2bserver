<?php

namespace App\Admin\Controllers;

use App\Admin\Actions\Grid\SiteProductCategorySyncAction;
use App\Admin\Actions\Grid\ToolAction\VendorRecordImportAction;
use App\Admin\Repositories\Site;
use App\Models\SitesCategory;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class SiteController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new Site('category'), function (Grid $grid) {
            $grid->column('id')->sortable();
            $grid->column('name')->editable(true);
            $grid->column('domian')->display(function ($domian) {
                return "<a href=$domian?_dreamtoken=$this->apipass target='_blank'>$domian</a>";
            })->help('点击网址跳转到网站后台管理');
            $grid->column('category.name', '分类');
            $grid->column('lastorder');
            $grid->column('memo')->editable(true);
            $grid->column('is_sync')->switch('info', true);
            $grid->column('inquiry_count');
            $grid->column('product_count');

            $grid->actions(new SiteProductCategorySyncAction());

            $grid->tools([
                new VendorRecordImportAction()
            ]);

            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('id');

            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new Site(), function (Show $show) {
            $show->field('id');
            $show->field('name');
            $show->field('domian');
            $show->field('category_id');
            $show->field('is_closed');
            $show->field('memo');
            $show->field('apipass');
            $show->field('newruler');
            $show->field('extra');
            $show->field('lastorder');
            $show->field('is_sync');
            $show->field('pinterest');
            $show->field('feed');
            $show->field('inquiry_count');
            $show->field('product_count');
            $show->field('checkonline');
            $show->field('checktime');
            $show->field('server_id');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new Site(), function (Form $form) {
            $form->display('id');
            $form->text('name');
            $form->text('domian');
            $form->select('category_id')->options(SitesCategory::selectOptions());
            $form->switch('is_sync')->customFormat(function ($v) {
                return $v == '1' ? 1 : 0;
            })->saving(function ($v) {
                return $v ? '1' : '0';
            })->default(1);
            $form->textarea('memo')->rows(3);
            $form->text('apipass');
            $form->text('newruler');
            $form->text('extra');
            $form->text('lastorder');
            $form->text('is_sync');
            $form->text('pinterest');
            $form->textarea('feed')->rows(8);
            $form->text('inquiry_count');
            $form->text('product_count');
            $form->text('checkonline');
            $form->text('checktime');
            $form->text('server_id');

            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
