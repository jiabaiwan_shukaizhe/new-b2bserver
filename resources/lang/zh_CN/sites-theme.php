<?php 
return [
    'labels' => [
        'SitesTheme' => '网站主题',
        'sites-theme' => '网站主题',
    ],
    'fields' => [
        'name' => '主题名称',
        'description' => '主题描述',
        'rules' => '主题规则',
        'termination' => '过期时间',
    ],
    'options' => [
    ],
];
