<?php
return [
    'labels' => [
        'StaffDepartment' => '部门',
        'staff-department' => '部门',
    ],
    'fields' => [
        'name' => '部门名称',
        'description' => '描述',
        'parent_id' => '父级ID',
        'orders' => '出单总量',
        'focus_tag' => '重点标签',
        'parent' => [
            'name' => '父级部门'
        ],
    ],
    'options' => [
    ],
];
