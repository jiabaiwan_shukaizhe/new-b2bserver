<?php
return [
    'labels' => [
        'SitesInquiry' => '询盘列表',
        'sites-inquiry' => '询盘列表',
    ],
    'fields' => [
        'siteid' => '网站id',
        'inquiry_id' => '询盘单id',
        'inquiry_state' => '状态',
        'is_trash' => '是否垃圾询盘单',
        'customer_realname' => '姓名',
        'email' => '客户邮箱',
        'phone' => '客户手机号',
        'inquiry_info' => '询盘单信息',
        'inquiry_quantity' => '询盘数量',
        'form_agent' => 'UA',
        'user_ipaddr' => 'ip地址',
        'user_ipaddr_type' => 'ip类型',
        'business_note' => '业务备注',
        'addtime' => '询盘单添加时间',
        'source_url' => '询盘单来源网址',
        'create_by' => '询盘单创建人',
        'operator_by' => '询盘单操作人',
        'source_country' => '国家',
    ],
    'options' => [
    ],
];
