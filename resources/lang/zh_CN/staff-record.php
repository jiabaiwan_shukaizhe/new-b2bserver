<?php 
return [
    'labels' => [
        'StaffRecord' => '雇员',
        'staff-record' => '雇员',
    ],
    'fields' => [
        'name' => '名字',
        'department_id' => '部门',
        'gender' => '性别',
        'title' => '职位',
        'mobile' => '手机',
        'email' => '邮箱',
        'ma_tag' => '经理标签',
    ],
    'options' => [
    ],
];
