<?php
return [
    'labels' => [
        'SitesCategory' => '网站类型',
        'sites-category' => '网站类型',
        'Parent' => '父级分类',
        'Themes' => '主题',
    ],
    'fields' => [
        'name' => '分类名称',
        'description' => '分类描述',
        'site_theme_ids' => '网站主题',
        'parent' => [
            'name' => '父级分类'
        ],
        'order' => '层级',
    ],
    'options' => [
    ],
];
