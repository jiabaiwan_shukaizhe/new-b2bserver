<?php
return [
    'labels' => [
        'Site' => '网站管理',
        'site' => '网站管理',
    ],
    'fields' => [
        'name' => '名称',
        'domian' => '域名',
        'sitetype' => '类型',
        'closed' => '是否关闭',
        'memo' => '备注',
        'addtime' => '创建时间',
        'apipass' => '接口密码',
        'newruler' => '路由规则',
        'extra' => '自定义',
        'lastorder' => '最后询盘',
        'is_sync' => '同步数据',
        'feed' => '模板',
        'inquiry_count' => '询盘单数',
        'product_count' => '产品数',
        'checkonline' => '是否在线',
        'checktime' => '最近一次检查时间',
        'serverid' => '服务器id',
    ],
    'options' => [
    ],
];
