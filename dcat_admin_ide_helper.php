<?php

/**
 * A helper file for Dcat Admin, to provide autocomplete information to your IDE
 *
 * This file should not be included in your code, only analyzed by your IDE!
 *
 * @author jqh <841324345@qq.com>
 */
namespace Dcat\Admin {
    use Illuminate\Support\Collection;

    /**
     * @property Grid\Column|Collection id
     * @property Grid\Column|Collection name
     * @property Grid\Column|Collection type
     * @property Grid\Column|Collection version
     * @property Grid\Column|Collection detail
     * @property Grid\Column|Collection created_at
     * @property Grid\Column|Collection updated_at
     * @property Grid\Column|Collection is_enabled
     * @property Grid\Column|Collection parent_id
     * @property Grid\Column|Collection order
     * @property Grid\Column|Collection icon
     * @property Grid\Column|Collection uri
     * @property Grid\Column|Collection extension
     * @property Grid\Column|Collection permission_id
     * @property Grid\Column|Collection menu_id
     * @property Grid\Column|Collection slug
     * @property Grid\Column|Collection http_method
     * @property Grid\Column|Collection http_path
     * @property Grid\Column|Collection role_id
     * @property Grid\Column|Collection user_id
     * @property Grid\Column|Collection value
     * @property Grid\Column|Collection username
     * @property Grid\Column|Collection password
     * @property Grid\Column|Collection avatar
     * @property Grid\Column|Collection remember_token
     * @property Grid\Column|Collection uuid
     * @property Grid\Column|Collection connection
     * @property Grid\Column|Collection queue
     * @property Grid\Column|Collection payload
     * @property Grid\Column|Collection exception
     * @property Grid\Column|Collection failed_at
     * @property Grid\Column|Collection email
     * @property Grid\Column|Collection token
     * @property Grid\Column|Collection domian
     * @property Grid\Column|Collection category_id
     * @property Grid\Column|Collection is_closed
     * @property Grid\Column|Collection memo
     * @property Grid\Column|Collection apipass
     * @property Grid\Column|Collection newruler
     * @property Grid\Column|Collection extra
     * @property Grid\Column|Collection lastorder
     * @property Grid\Column|Collection is_sync
     * @property Grid\Column|Collection pinterest
     * @property Grid\Column|Collection feed
     * @property Grid\Column|Collection inquiry_count
     * @property Grid\Column|Collection product_count
     * @property Grid\Column|Collection checkonline
     * @property Grid\Column|Collection checktime
     * @property Grid\Column|Collection server_id
     * @property Grid\Column|Collection deleted_at
     * @property Grid\Column|Collection site_theme_ids
     * @property Grid\Column|Collection site_id
     * @property Grid\Column|Collection inquiry_id
     * @property Grid\Column|Collection inquiry_state
     * @property Grid\Column|Collection is_trash
     * @property Grid\Column|Collection customer_realname
     * @property Grid\Column|Collection phone
     * @property Grid\Column|Collection inquiry_info
     * @property Grid\Column|Collection inquiry_quantity
     * @property Grid\Column|Collection form_agent
     * @property Grid\Column|Collection user_ipaddr
     * @property Grid\Column|Collection user_ipaddr_type
     * @property Grid\Column|Collection business_note
     * @property Grid\Column|Collection addtime
     * @property Grid\Column|Collection source_url
     * @property Grid\Column|Collection source_country
     * @property Grid\Column|Collection create_by
     * @property Grid\Column|Collection operator_by
     * @property Grid\Column|Collection rules
     * @property Grid\Column|Collection termination
     * @property Grid\Column|Collection focus_tag
     * @property Grid\Column|Collection department_id
     * @property Grid\Column|Collection gender
     * @property Grid\Column|Collection mobile
     * @property Grid\Column|Collection ma_tag
     * @property Grid\Column|Collection email_verified_at
     *
     * @method Grid\Column|Collection id(string $label = null)
     * @method Grid\Column|Collection name(string $label = null)
     * @method Grid\Column|Collection type(string $label = null)
     * @method Grid\Column|Collection version(string $label = null)
     * @method Grid\Column|Collection detail(string $label = null)
     * @method Grid\Column|Collection created_at(string $label = null)
     * @method Grid\Column|Collection updated_at(string $label = null)
     * @method Grid\Column|Collection is_enabled(string $label = null)
     * @method Grid\Column|Collection parent_id(string $label = null)
     * @method Grid\Column|Collection order(string $label = null)
     * @method Grid\Column|Collection icon(string $label = null)
     * @method Grid\Column|Collection uri(string $label = null)
     * @method Grid\Column|Collection extension(string $label = null)
     * @method Grid\Column|Collection permission_id(string $label = null)
     * @method Grid\Column|Collection menu_id(string $label = null)
     * @method Grid\Column|Collection slug(string $label = null)
     * @method Grid\Column|Collection http_method(string $label = null)
     * @method Grid\Column|Collection http_path(string $label = null)
     * @method Grid\Column|Collection role_id(string $label = null)
     * @method Grid\Column|Collection user_id(string $label = null)
     * @method Grid\Column|Collection value(string $label = null)
     * @method Grid\Column|Collection username(string $label = null)
     * @method Grid\Column|Collection password(string $label = null)
     * @method Grid\Column|Collection avatar(string $label = null)
     * @method Grid\Column|Collection remember_token(string $label = null)
     * @method Grid\Column|Collection uuid(string $label = null)
     * @method Grid\Column|Collection connection(string $label = null)
     * @method Grid\Column|Collection queue(string $label = null)
     * @method Grid\Column|Collection payload(string $label = null)
     * @method Grid\Column|Collection exception(string $label = null)
     * @method Grid\Column|Collection failed_at(string $label = null)
     * @method Grid\Column|Collection email(string $label = null)
     * @method Grid\Column|Collection token(string $label = null)
     * @method Grid\Column|Collection domian(string $label = null)
     * @method Grid\Column|Collection category_id(string $label = null)
     * @method Grid\Column|Collection is_closed(string $label = null)
     * @method Grid\Column|Collection memo(string $label = null)
     * @method Grid\Column|Collection apipass(string $label = null)
     * @method Grid\Column|Collection newruler(string $label = null)
     * @method Grid\Column|Collection extra(string $label = null)
     * @method Grid\Column|Collection lastorder(string $label = null)
     * @method Grid\Column|Collection is_sync(string $label = null)
     * @method Grid\Column|Collection pinterest(string $label = null)
     * @method Grid\Column|Collection feed(string $label = null)
     * @method Grid\Column|Collection inquiry_count(string $label = null)
     * @method Grid\Column|Collection product_count(string $label = null)
     * @method Grid\Column|Collection checkonline(string $label = null)
     * @method Grid\Column|Collection checktime(string $label = null)
     * @method Grid\Column|Collection server_id(string $label = null)
     * @method Grid\Column|Collection deleted_at(string $label = null)
     * @method Grid\Column|Collection site_theme_ids(string $label = null)
     * @method Grid\Column|Collection site_id(string $label = null)
     * @method Grid\Column|Collection inquiry_id(string $label = null)
     * @method Grid\Column|Collection inquiry_state(string $label = null)
     * @method Grid\Column|Collection is_trash(string $label = null)
     * @method Grid\Column|Collection customer_realname(string $label = null)
     * @method Grid\Column|Collection phone(string $label = null)
     * @method Grid\Column|Collection inquiry_info(string $label = null)
     * @method Grid\Column|Collection inquiry_quantity(string $label = null)
     * @method Grid\Column|Collection form_agent(string $label = null)
     * @method Grid\Column|Collection user_ipaddr(string $label = null)
     * @method Grid\Column|Collection user_ipaddr_type(string $label = null)
     * @method Grid\Column|Collection business_note(string $label = null)
     * @method Grid\Column|Collection addtime(string $label = null)
     * @method Grid\Column|Collection source_url(string $label = null)
     * @method Grid\Column|Collection source_country(string $label = null)
     * @method Grid\Column|Collection create_by(string $label = null)
     * @method Grid\Column|Collection operator_by(string $label = null)
     * @method Grid\Column|Collection rules(string $label = null)
     * @method Grid\Column|Collection termination(string $label = null)
     * @method Grid\Column|Collection focus_tag(string $label = null)
     * @method Grid\Column|Collection department_id(string $label = null)
     * @method Grid\Column|Collection gender(string $label = null)
     * @method Grid\Column|Collection mobile(string $label = null)
     * @method Grid\Column|Collection ma_tag(string $label = null)
     * @method Grid\Column|Collection email_verified_at(string $label = null)
     */
    class Grid {}

    class MiniGrid extends Grid {}

    /**
     * @property Show\Field|Collection id
     * @property Show\Field|Collection name
     * @property Show\Field|Collection type
     * @property Show\Field|Collection version
     * @property Show\Field|Collection detail
     * @property Show\Field|Collection created_at
     * @property Show\Field|Collection updated_at
     * @property Show\Field|Collection is_enabled
     * @property Show\Field|Collection parent_id
     * @property Show\Field|Collection order
     * @property Show\Field|Collection icon
     * @property Show\Field|Collection uri
     * @property Show\Field|Collection extension
     * @property Show\Field|Collection permission_id
     * @property Show\Field|Collection menu_id
     * @property Show\Field|Collection slug
     * @property Show\Field|Collection http_method
     * @property Show\Field|Collection http_path
     * @property Show\Field|Collection role_id
     * @property Show\Field|Collection user_id
     * @property Show\Field|Collection value
     * @property Show\Field|Collection username
     * @property Show\Field|Collection password
     * @property Show\Field|Collection avatar
     * @property Show\Field|Collection remember_token
     * @property Show\Field|Collection uuid
     * @property Show\Field|Collection connection
     * @property Show\Field|Collection queue
     * @property Show\Field|Collection payload
     * @property Show\Field|Collection exception
     * @property Show\Field|Collection failed_at
     * @property Show\Field|Collection email
     * @property Show\Field|Collection token
     * @property Show\Field|Collection domian
     * @property Show\Field|Collection category_id
     * @property Show\Field|Collection is_closed
     * @property Show\Field|Collection memo
     * @property Show\Field|Collection apipass
     * @property Show\Field|Collection newruler
     * @property Show\Field|Collection extra
     * @property Show\Field|Collection lastorder
     * @property Show\Field|Collection is_sync
     * @property Show\Field|Collection pinterest
     * @property Show\Field|Collection feed
     * @property Show\Field|Collection inquiry_count
     * @property Show\Field|Collection product_count
     * @property Show\Field|Collection checkonline
     * @property Show\Field|Collection checktime
     * @property Show\Field|Collection server_id
     * @property Show\Field|Collection deleted_at
     * @property Show\Field|Collection site_theme_ids
     * @property Show\Field|Collection site_id
     * @property Show\Field|Collection inquiry_id
     * @property Show\Field|Collection inquiry_state
     * @property Show\Field|Collection is_trash
     * @property Show\Field|Collection customer_realname
     * @property Show\Field|Collection phone
     * @property Show\Field|Collection inquiry_info
     * @property Show\Field|Collection inquiry_quantity
     * @property Show\Field|Collection form_agent
     * @property Show\Field|Collection user_ipaddr
     * @property Show\Field|Collection user_ipaddr_type
     * @property Show\Field|Collection business_note
     * @property Show\Field|Collection addtime
     * @property Show\Field|Collection source_url
     * @property Show\Field|Collection source_country
     * @property Show\Field|Collection create_by
     * @property Show\Field|Collection operator_by
     * @property Show\Field|Collection rules
     * @property Show\Field|Collection termination
     * @property Show\Field|Collection focus_tag
     * @property Show\Field|Collection department_id
     * @property Show\Field|Collection gender
     * @property Show\Field|Collection mobile
     * @property Show\Field|Collection ma_tag
     * @property Show\Field|Collection email_verified_at
     *
     * @method Show\Field|Collection id(string $label = null)
     * @method Show\Field|Collection name(string $label = null)
     * @method Show\Field|Collection type(string $label = null)
     * @method Show\Field|Collection version(string $label = null)
     * @method Show\Field|Collection detail(string $label = null)
     * @method Show\Field|Collection created_at(string $label = null)
     * @method Show\Field|Collection updated_at(string $label = null)
     * @method Show\Field|Collection is_enabled(string $label = null)
     * @method Show\Field|Collection parent_id(string $label = null)
     * @method Show\Field|Collection order(string $label = null)
     * @method Show\Field|Collection icon(string $label = null)
     * @method Show\Field|Collection uri(string $label = null)
     * @method Show\Field|Collection extension(string $label = null)
     * @method Show\Field|Collection permission_id(string $label = null)
     * @method Show\Field|Collection menu_id(string $label = null)
     * @method Show\Field|Collection slug(string $label = null)
     * @method Show\Field|Collection http_method(string $label = null)
     * @method Show\Field|Collection http_path(string $label = null)
     * @method Show\Field|Collection role_id(string $label = null)
     * @method Show\Field|Collection user_id(string $label = null)
     * @method Show\Field|Collection value(string $label = null)
     * @method Show\Field|Collection username(string $label = null)
     * @method Show\Field|Collection password(string $label = null)
     * @method Show\Field|Collection avatar(string $label = null)
     * @method Show\Field|Collection remember_token(string $label = null)
     * @method Show\Field|Collection uuid(string $label = null)
     * @method Show\Field|Collection connection(string $label = null)
     * @method Show\Field|Collection queue(string $label = null)
     * @method Show\Field|Collection payload(string $label = null)
     * @method Show\Field|Collection exception(string $label = null)
     * @method Show\Field|Collection failed_at(string $label = null)
     * @method Show\Field|Collection email(string $label = null)
     * @method Show\Field|Collection token(string $label = null)
     * @method Show\Field|Collection domian(string $label = null)
     * @method Show\Field|Collection category_id(string $label = null)
     * @method Show\Field|Collection is_closed(string $label = null)
     * @method Show\Field|Collection memo(string $label = null)
     * @method Show\Field|Collection apipass(string $label = null)
     * @method Show\Field|Collection newruler(string $label = null)
     * @method Show\Field|Collection extra(string $label = null)
     * @method Show\Field|Collection lastorder(string $label = null)
     * @method Show\Field|Collection is_sync(string $label = null)
     * @method Show\Field|Collection pinterest(string $label = null)
     * @method Show\Field|Collection feed(string $label = null)
     * @method Show\Field|Collection inquiry_count(string $label = null)
     * @method Show\Field|Collection product_count(string $label = null)
     * @method Show\Field|Collection checkonline(string $label = null)
     * @method Show\Field|Collection checktime(string $label = null)
     * @method Show\Field|Collection server_id(string $label = null)
     * @method Show\Field|Collection deleted_at(string $label = null)
     * @method Show\Field|Collection site_theme_ids(string $label = null)
     * @method Show\Field|Collection site_id(string $label = null)
     * @method Show\Field|Collection inquiry_id(string $label = null)
     * @method Show\Field|Collection inquiry_state(string $label = null)
     * @method Show\Field|Collection is_trash(string $label = null)
     * @method Show\Field|Collection customer_realname(string $label = null)
     * @method Show\Field|Collection phone(string $label = null)
     * @method Show\Field|Collection inquiry_info(string $label = null)
     * @method Show\Field|Collection inquiry_quantity(string $label = null)
     * @method Show\Field|Collection form_agent(string $label = null)
     * @method Show\Field|Collection user_ipaddr(string $label = null)
     * @method Show\Field|Collection user_ipaddr_type(string $label = null)
     * @method Show\Field|Collection business_note(string $label = null)
     * @method Show\Field|Collection addtime(string $label = null)
     * @method Show\Field|Collection source_url(string $label = null)
     * @method Show\Field|Collection source_country(string $label = null)
     * @method Show\Field|Collection create_by(string $label = null)
     * @method Show\Field|Collection operator_by(string $label = null)
     * @method Show\Field|Collection rules(string $label = null)
     * @method Show\Field|Collection termination(string $label = null)
     * @method Show\Field|Collection focus_tag(string $label = null)
     * @method Show\Field|Collection department_id(string $label = null)
     * @method Show\Field|Collection gender(string $label = null)
     * @method Show\Field|Collection mobile(string $label = null)
     * @method Show\Field|Collection ma_tag(string $label = null)
     * @method Show\Field|Collection email_verified_at(string $label = null)
     */
    class Show {}

    /**
     * @method \Dcat\Admin\Extension\EasySku\SkuField sku(...$params)
     */
    class Form {}

}

namespace Dcat\Admin\Grid {
    /**
     
     */
    class Column {}

    /**
     
     */
    class Filter {}
}

namespace Dcat\Admin\Show {
    /**
     
     */
    class Field {}
}
