<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff_records', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->default('')->comment('名字');
            $table->integer('department_id')->comment('部门');
            $table->char('gender')->default('未知')->comment('性别');
            $table->string('title')->nullable()->comment('职位');
            $table->string('mobile')->nullable()->comment('手机');
            $table->string('email')->nullable()->comment('邮箱');
            $table->integer('ma_tag')->default('0')->comment('经理标签');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_records');
    }
}
