<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSitesThemesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sites_themes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->default('')->comment('主题名称');
            $table->string('description')->nullable()->comment('主题描述');
            $table->longText('rules')->comment('主题规则');
            $table->date('termination')->nullable()->comment('过期时间');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sites_themes');
    }
}
