<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sites', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable()->comment('名称');
            $table->string('domian')->nullable()->comment('域名');
            $table->integer('category_id')->default('0')->nullable()->comment('分类');
            $table->tinyInteger('is_closed')->default('0')->nullable()->comment('关闭网站');
            $table->text('memo')->nullable()->comment('备注');
            $table->string('apipass')->nullable()->comment('API密钥');
            $table->string('newruler')->nullable();
            $table->text('extra')->nullable();
            $table->dateTime('lastorder')->nullable()->comment('备注');
            $table->tinyInteger('is_sync')->default('1')->nullable()->comment('同步数据');
            $table->string('pinterest')->nullable()->comment('产品页模板');
            $table->string('feed')->nullable();
            $table->integer('inquiry_count')->default('0')->nullable()->comment('询盘数');
            $table->integer('product_count')->default('0')->nullable()->comment('商品数');
            $table->tinyInteger('checkonline')->default('1')->nullable()->comment('是否在线');
            $table->dateTime('checktime')->nullable()->comment('检查时间');
            $table->integer('server_id')->default('0')->nullable()->comment('服务器id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sites');
    }
}
