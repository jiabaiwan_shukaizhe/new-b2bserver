<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff_departments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->default('')->comment('部门名称');
            $table->string('description')->nullable()->comment('描述');
            $table->integer('parent_id')->nullable()->comment('父级ID');
            $table->string('orders')->default('0')->comment('出单总量');
            $table->integer('focus_tag')->default('0')->comment('重点标签');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_departments');
    }
}
