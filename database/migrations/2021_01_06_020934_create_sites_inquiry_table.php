<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSitesInquiryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sites_inquiry', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('siteid')->comment('网站id');
            $table->bigInteger('inquiry_id')->comment('询盘单id');
            $table->tinyInteger('inquiry_state')->comment('询盘单状态');
            $table->tinyInteger('is_trash')->comment('是否垃圾询盘单，1：不是，2：是');
            $table->string('customer_realname')->default('')->comment('客户真实姓名');
            $table->string('email')->default('')->comment('客户邮箱');
            $table->string('phone')->default('')->comment('客户手机号');
            $table->text('inquiry_info')->comment('询盘单信息');
            $table->string('inquiry_quantity')->default('')->comment('询盘数量');
            $table->string('form_agent')->default('')->comment('表单UA');
            $table->string('user_ipaddr')->default('')->comment('用户ip地址');
            $table->string('user_ipaddr_type')->default('')->comment('用户ip类型');
            $table->text('business_note')->comment('业务员备注');
            $table->dateTime('addtime')->comment('询盘单添加时间');
            $table->string('source_url')->default('')->comment('询盘单来源网址');
            $table->string('create_by')->default('')->comment('询盘单创建人');
            $table->string('operator_by')->default('')->comment('询盘单操作人');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sites_inquiry');
    }
}
